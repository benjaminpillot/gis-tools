import networkx as nx

from matplotlib import pyplot as plt

from gistools.geometry import polygon_collection_to_graph, build_partitions
from gistools.layer import PolygonLayer

pop_density = PolygonLayer("/home/benjamin/Documents/PRO/PROJETS/BOULANGERIE_SOLAIRE/001_DATA"
                           "/pop_density_250m_RUN.shp")
iris_size = 10000

my_iris = pop_density.build_partitions(iris_size, "POPULATION", contig=True, ncuts=10,
                                       niter=10, objtype="cut", ufactor=10)
# my_iris = pop_density.build_partitions(iris_size, "POPULATION", contig=True)

# total_pop = pop_density["POPULATION"].sum()
# pop_per_part = 3000
# nparts = int(total_pop/pop_per_part)
# tpweights = [(d,) for d in [pop_per_part/total_pop] * nparts]
# weights = pop_density["POPULATION"].astype(int)

# iris = PolygonLayer("/home/benjamin/Documents/PRO/THESES/THESE_ROMAIN/001_DATA/POPULATION"
#                     "/iris_pop_reunion.shp")

# pop_density_graph = polygon_collection_to_graph(pop_density.geometry,
#                                                 weights=pop_density["POPULATION"])

# pop_density_parts = build_partitions(pop_density.geometry, weights,
#                                      nparts=nparts, tpweights=tpweights,
#                                      weight_attr="POPULATION", recursive=True,
#                                      contig=True)
#
# my_iris = PolygonLayer.from_collection(pop_density_parts)

my_iris.to_file(f"/home/benjamin/Documents/PRO/PROJETS/BOULANGERIE_SOLAIRE/001_DATA"
                f"/my_iris_{iris_size}_grid_250m.shp")

# iris_graph = polygon_collection_to_graph(iris.geometry, weights=iris["POPULATION"])
# nx.draw(iris_graph)

# plt.draw()
# plt.show()

